<?php

class WordPressSubfolderValetDriver extends BasicValetDriver
{
    /**
     * Determine if the driver serves the request.
     *
     * @param  string  $sitePath
     * @param  string  $siteName
     * @param  string  $uri
     * @return bool
     */
    public function serves($sitePath, $siteName, $uri)
    {        
        // If Subfolder NU Bedrock site
        if ($siteName !== 'northeastern') return false;

        // If Subfolder NU WP site
        $subDirectory = explode('/', $uri);
        $sitePath = ($siteName == 'northeastern' && !empty($subDirectory[1]) 
            ? $sitePath.'/'.$subDirectory[1] 
            : $sitePath);

        return file_exists($sitePath.'/wp-config.php');
    }

    /**
     * Get the fully resolved path to the application's front controller.
     *
     * @param  string  $sitePath
     * @param  string  $siteName
     * @param  string  $uri
     * @return string
     */
    public function frontControllerPath($sitePath, $siteName, $uri)
    {
        $subDirectory = explode('/', $uri);
        $sitePath = $sitePath.'/'.$subDirectory[1]; // add "subfolder" to $sitePath
        $uri = '/'. implode("/",array_slice($subDirectory,2)); // drop "subfolder" and put uri back together

        $_SERVER['PHP_SELF']    = $uri;
        $_SERVER['SERVER_ADDR'] = '127.0.0.1';

        return parent::frontControllerPath($sitePath, $siteName, $uri);
    }
}