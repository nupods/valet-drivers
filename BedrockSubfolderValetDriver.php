<?php

class BedrockSubfolderValetDriver extends BasicValetDriver
{
    /**
     * Determine if the driver serves the request.
     *
     * @param  string  $sitePath
     * @param  string  $siteName
     * @param  string  $uri
     * @return bool
     */
    public function serves($sitePath, $siteName, $uri)
    {
        // If Subfolder NU Bedrock site
        if ($siteName !== 'northeastern') return false;

        $subDirectory = explode('/', $uri);
        $sitePath = ($siteName == 'northeastern' && !empty($subDirectory[1]) 
            ? $sitePath.'/'.$subDirectory[1] 
            : $sitePath);

        return file_exists($sitePath.'/web/app/mu-plugins/bedrock-autoloader.php') ||
              (is_dir($sitePath.'/web/app/') &&
               file_exists($sitePath.'/web/wp-config.php') &&
               file_exists($sitePath.'/config/application.php'));
    }

    /**
     * Determine if the incoming request is for a static file.
     *
     * @param  string  $sitePath
     * @param  string  $siteName
     * @param  string  $uri
     * @return string|false
     */
    public function isStaticFile($sitePath, $siteName, $uri)
    {
        $subDirectory = explode('/', $uri);
        $sitePath = $sitePath.'/'.$subDirectory[1]; // add "subfolder" to $sitePath
        $uri = '/'. implode("/",array_slice($subDirectory,2)); // drop "subfolder" and put uri back together


        $staticFilePath = $sitePath.'/web'.$uri;

        if ($this->isActualFile($staticFilePath)) {
            return $staticFilePath;
        }
        return false;
    }

    /**
     * Get the fully resolved path to the application's front controller.
     *
     * @param  string  $sitePath
     * @param  string  $siteName
     * @param  string  $uri
     * @return string
     */
    public function frontControllerPath($sitePath, $siteName, $uri)
    {
        $subDirectory = explode('/', $uri);
        $sitePath = $sitePath.'/'.$subDirectory[1]; // add "subfolder" to $sitePath
        $uri = '/'. implode("/",array_slice($subDirectory,2)); // drop "subfolder" and put uri back together


        $_SERVER['PHP_SELF'] = $uri;

        if (strpos($uri, '/wp/') === 0) {
            return is_dir($sitePath.'/web'.$uri)
                            ? $sitePath.'/web'.$uri.'/index.php'
                            : $sitePath.'/web'.$uri;
        }

        return $sitePath.'/web/index.php';
    }
}